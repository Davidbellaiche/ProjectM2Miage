package com.example.locationVoiture;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


/// Partie Controleur
@Controller
public class MyController {
	CustomerService customerserv = new CustomerService();
	
	
	
	
	@RequestMapping(value = "/cars/{plateNumber}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	
	///Méthode Louer
	public void Louer(@PathVariable("plateNumber") String plateNumber,@RequestParam(value="En location", required = true)boolean louer) throws Exception{
		
		for(int i=0;i<customerserv.getListCar().size();i++){
			
			if(customerserv.getListCar().get(i).getPlatenumber().equals(plateNumber)){
			 customerserv.getListCar().get(i).setLouer(louer);
			}
				
			}
		
	}
	
	@RequestMapping(value = "/cars", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	///On créé une liste de voiture et on l'insère dans le customer service
	public List<Car> listOfCars(){
		return customerserv.getListCar();
	}
	
	@RequestMapping(value = "/cars/{plateNumber}", method = RequestMethod.GET)
	
	@ResponseStatus(HttpStatus.OK)
	
	@ResponseBody
	
	///Méthode pour retourner une voiture grâce à sa plaque d'immatriculation
	public Car SearchCar(@PathVariable("plateNumber") String plateNumber) throws Exception{
		Car car = null;
	
		for(int i=0;i<customerserv.getListCar().size();i++){
			
			if(customerserv.getListCar().get(i).getPlatenumber().equals(plateNumber)){
			car = customerserv.getListCar().get(i);
			}
			
		}
		return car;
	}
	@RequestMapping(value = "/cars/{plateNumber}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void getBack(@PathVariable("plateNumber") String plateNumber) throws Exception{
		
		for(int i=0;i<customerserv.getListCar().size();i++){
			
		if(customerserv.getListCar().get(i).getPlatenumber().equals(plateNumber)){
		 customerserv.getListCar().remove(i);
		}
			
		}
		
		
	}
	

}
