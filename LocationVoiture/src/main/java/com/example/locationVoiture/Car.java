package com.example.locationVoiture;

public class Car {
private String platenumber;
private String marque;
private String modele;
private boolean louer;

public Car(String Platenumber,String Marque,String Modele){
	setPlatenumber(Platenumber);
	setMarque(Marque);
	setModele(Modele);
	setLouer(false);
}

public String getPlatenumber() {
	return platenumber;
}
public void setPlatenumber(String platenumber) {
	this.platenumber = platenumber;
}
public String getMarque() {
	return marque;
}
public void setMarque(String marque) {
	this.marque = marque;
}
public String getModele() {
	return modele;
}
public void setModele(String modele) {
	this.modele = modele;
}

public boolean getLouer() {
	return louer;
}

public void setLouer(boolean louer) {
	this.louer = louer;
}

}
