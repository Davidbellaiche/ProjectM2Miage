package com.example.locationVoiture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocationvoitureWebservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(LocationvoitureWebservicesApplication.class, args);
	}
}
