package com.example.locationVoiture;

import java.util.ArrayList;
import java.util.List;

public class CustomerService {
	private List<Car> listCar;
	public  CustomerService(){
		listCar = new ArrayList<Car>();
		listCar.add(new Car("994 ERJ 92","Mercedes","CLS"));
	}
	public List<Car> getListCar() {
		return listCar;
	}
	public void setListCar(List<Car> listCar) {
		this.listCar = listCar;
	}
}
