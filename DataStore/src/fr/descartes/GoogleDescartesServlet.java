package fr.descartes;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.*;
import com.google.appengine.api.datastore.*;
import com.google.appengine.api.datastore.Query.SortDirection;

@SuppressWarnings("serial")
public class GoogleDescartesServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/plain");
		String nom = req.getParameter("nom");
		resp.getWriter().print("Bonjour "+nom+"et bienvenue !");
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Entity employee = new Entity("Employee");
		employee.setProperty("firstName", "Antonio"); 
		employee.setProperty("lastName", "Salieri"); 
		employee.setProperty("hireDate", new Date()); 
		employee.setProperty("attendedHrTraining", true);
		datastore.put(employee);
		
		Entity employee2 = new Entity("Employee2", "GROSTEST");
		Key key = KeyFactory.createKey("Employee", "asalieri");
		Key key2 = employee.getKey();
		//datastore.delete(key);
		datastore.put(employee2);
		
		Query q1 = new Query("Person").addSort("lastName",SortDirection.ASCENDING);
		
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		this.doGet(req,resp);
	}
}
